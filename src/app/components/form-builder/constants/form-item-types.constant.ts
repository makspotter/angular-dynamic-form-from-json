export enum FORM_ITEM_TYPES {
  BLOCK       = 'BLOCK',
  TEXT        = 'TEXT',
  TEXTAREA    = 'TEXTAREA',
  NUMBER      = 'NUMBER',
  DATE        = 'DATE',
  DROPDOWN    = 'DROPDOWN',
  RADIOBUTTON = 'RADIOBUTTON',
}
