import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs/internal/Subject';
import { delay, takeUntil } from 'rxjs/operators';

import { FORM_ITEM_TYPES } from './constants/form-item-types.constant';
import { FieldModel } from './models/field.model';
import { FormDataMapperService } from './services/form-data-mapper.service';
import { FormDataApiService } from './services/form-data.api.service';
import { TFormDataItems } from './types/form-data.type';

const FORM_DEBOUNCE = 300;

@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormBuilderComponent implements OnInit, OnDestroy {
  isLoading: boolean            = true;
  isPostedData: boolean         = false;
  formGroup: FormGroup;
  formDataItems: TFormDataItems = [];

  FORM_ITEM_TYPES: typeof FORM_ITEM_TYPES = FORM_ITEM_TYPES;

  private readonly _destroy$: Subject<void> = new Subject<void>();

  constructor (
    private _changeDetectorRef: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _formDataApiService: FormDataApiService,
    private _formDataMapperService: FormDataMapperService,
  ) {
  }

  ngOnInit (): void {
    this.formGroup = this._formBuilder.group({});

    this._formDataApiService.getData()
      .pipe(takeUntil(this._destroy$))
      .subscribe((formData) => {
        this.formDataItems = formData.form;

        this.addFormControls(this.formDataItems, this.formGroup);

        this.isLoading = false;

        this._changeDetectorRef.detectChanges();
      });
  }

  ngOnDestroy (): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  addFormControls (items: TFormDataItems, formGroup: FormGroup, isRequired: boolean = false): void {
    items.forEach(item => {
      if (item.type === FORM_ITEM_TYPES.BLOCK) {
        const childFormGroup = item.abstractControl as FormGroup;

        formGroup.addControl(item.id, childFormGroup);

        if (item.required) {
          childFormGroup.setValidators(Validators.required);
        }

        if (item.items && item.items.length) {
          this.addFormControls(item.items, childFormGroup, item.required);
        }

        return;
      }

      const fieldItem   = item as any as FieldModel;
      const formControl = item.abstractControl as FormControl;

      if (fieldItem.value) {
        formControl.setValue(fieldItem.value, { emitEvent: false });
      }

      if (isRequired || fieldItem.required) {
        formControl.setValidators(Validators.required);
      }

      formGroup.addControl(fieldItem.id, formControl);
    });
  }

  onSubmit (event: Event): void {
    if (!this.formGroup.valid) {
      this.formGroup.markAllAsTouched();

      return;
    }

    event.preventDefault();

    this.isPostedData = true;
    this._changeDetectorRef.detectChanges();

    const mappedFormValues = this._formDataMapperService.mapData(
      this.formGroup.getRawValue(),
      [...this.formDataItems],
    );

    this._formDataApiService.postData({ form: mappedFormValues })
      .pipe(
        takeUntil(this._destroy$),
        delay(1500), // emulate Backend timeout
      )
      .subscribe((data) => {
        this.isPostedData = false;

        console.info(data); // posted form data

        this._changeDetectorRef.detectChanges();
      });
  }
}
