import { BlockModel } from '../models/block.model';
import { FieldModel } from '../models/field.model';

export type TFormDataItems = Array<BlockModel | FieldModel>;
