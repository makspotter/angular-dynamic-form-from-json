import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule, MatOptionModule,
  MatProgressSpinnerModule, MatRadioModule, MatSelectModule,
} from '@angular/material';

import { FormBuilderComponent } from './form-builder.component';
import { FormDataMapperService } from './services/form-data-mapper.service';
import { FormDataApiService } from './services/form-data.api.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatSelectModule,
    MatOptionModule,
    MatRadioModule,
  ],
  exports:[
    FormBuilderComponent,
  ],
  declarations: [
    FormBuilderComponent,
  ],
  providers: [
    FormDataApiService,
    FormDataMapperService,
  ],
})
export class FormBuilderModule {}
