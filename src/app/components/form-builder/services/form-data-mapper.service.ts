import { Injectable } from '@angular/core';

import { BlockModel } from '../models/block.model';
import { FieldModel } from '../models/field.model';
import { TFormDataItems } from '../types/form-data.type';

@Injectable()
export class FormDataMapperService {
  mapData (rawValue: any, data: TFormDataItems): TFormDataItems {
    const flattenFormData = this.flattObjectData(rawValue);

    this.setValue(flattenFormData, data);

    return data;
  }

  // Flatten raw values - get only form field values
  private flattObjectData (data: any): any {
    const flattened = {};

    Object.keys(data).forEach((key) => {
      if (data[key] !== null && data[key].constructor.name === 'Object') {
        Object.assign(flattened, this.flattObjectData(data[key]));
      } else {
        flattened[key] = data[key];
      }
    });

    return flattened;
  }

  private setValue (flattenFormData: any, data: any): any {
    data.forEach(item => {
      if (item instanceof FieldModel) {
        item.value = flattenFormData[item.id];
      }

      if (item instanceof BlockModel && item.items) {
        this.setValue(flattenFormData, item.items);
      }
    });
  }
}
