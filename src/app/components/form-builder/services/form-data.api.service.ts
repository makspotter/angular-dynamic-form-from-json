import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';
import { map } from 'rxjs/operators';

import { FormDataModel } from '../models/form-data.model';

@Injectable()
export class FormDataApiService {
  constructor (private _httpClient: HttpClient) {
  }

  getData (): Observable<FormDataModel> {
    return this._httpClient.get<FormDataModel>('assets/data.json')
      .pipe(
        map(data => new FormDataModel(data)),
      );
  }

  postData (formData: FormDataModel): Observable<FormDataModel> {
    return of(formData);
  }
}
