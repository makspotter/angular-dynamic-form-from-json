import { AbstractControl, FormControl } from '@angular/forms';

import { FORM_ITEM_TYPES } from '../constants/form-item-types.constant';

import { FieldValueModel } from './field-value.model';

export class FieldModel {
  id: string;
  type: typeof FORM_ITEM_TYPES;
  required?: boolean;
  placeholder: string;
  multiple?: boolean;
  value?: string | number | string[];
  values?: FieldValueModel[];
  abstractControl: AbstractControl;

  constructor (data: Partial<FieldModel> = {}) {
    Object.assign(this, data);

    this.abstractControl = new FormControl();

    if (data.values) {
      this.values = data.values.map(value => new FieldValueModel(value));
    }
  }
}
