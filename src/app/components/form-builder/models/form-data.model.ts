import { FORM_ITEM_TYPES } from '../constants/form-item-types.constant';
import { TFormDataItems } from '../types/form-data.type';

import { BlockModel } from './block.model';
import { FieldModel } from './field.model';

export class FormDataModel {
  form: TFormDataItems;

  constructor (data: Partial<FormDataModel> = {}) {
    Object.assign(this, data, {
      form: data.form.map((it: BlockModel & FieldModel) => {
        return it.type === FORM_ITEM_TYPES.BLOCK
          ? new BlockModel(it)
          : new FieldModel(it);
      }),
    });
  }
}
