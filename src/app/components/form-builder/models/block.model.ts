import { AbstractControl, FormGroup } from '@angular/forms';

import { FORM_ITEM_TYPES } from '../constants/form-item-types.constant';
import { TFormDataItems } from '../types/form-data.type';

import { FieldModel } from './field.model';

export class BlockModel {
  id: string;
  type: FORM_ITEM_TYPES;
  required?: boolean;
  items: TFormDataItems;
  abstractControl: AbstractControl;

  constructor (data: Partial<BlockModel> = {}) {
    Object.assign(this, data);

    this.abstractControl = new FormGroup({});

    if (data.items) {
      this.items = data.items.map((it: BlockModel & FieldModel) => {
        return it.type === FORM_ITEM_TYPES.BLOCK
          ? new BlockModel(it)
          : new FieldModel(it);
      });
    }
  }
}
