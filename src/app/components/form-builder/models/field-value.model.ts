export class FieldValueModel {
  id: string;
  key: string;
  value: string | number;

  constructor (data: Partial<FieldValueModel> = {}) {
    Object.assign(this, data);
  }
}
